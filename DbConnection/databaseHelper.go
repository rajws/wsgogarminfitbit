package DbConnection

import (
	"database/sql"
	"fmt"
)

const (
	//DbHost     = "localhost"

	//DbName1 : database name
	DbName1 = "admin_fitness_data"

	//DbUser1 : Database user
	DbUser1 = "sys-production"

	//DbPassword : Database Password
	DbPassword = "j29.!kl;2mzo8qomo.aslignyz;742ng2j:1u87<,1aq"
)

var db1 sql.DB

//OpenDB will open the database connection
func OpenDB() *sql.DB {
	//db, err := sql.Open("mysql", fmt.Sprintf("%s/%s/%s", DB_NAME, DB_USER, DB_PASS))
	db1, err := sql.Open("mysql", DbUser1+":"+DbPassword+"@/"+DbName1)
	db1.SetMaxIdleConns(100)
	if err != nil {
		fmt.Println("error in opening database")
	}
	return db1
}

//CloseDB will close the database connection
func CloseDB() {
	db1.Close()
}
