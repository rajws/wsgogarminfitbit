package DbConnection

import (
	"WSGoGarminFitbit/ErrorExit"
	"database/sql"

	"fmt"
	"sync"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB
var once sync.Once

const (
	//DbHost point to staging database after running ssh command, that db will run on 3307 port
	DbHost = "tcp(127.0.0.1:3307)"
	//DbName is Staging database name
	DbName = "admin_fitness_data"
	//DbUser is Username of mysql
	DbUser = "sys-production"
	//DbPass is password of mysql staging database
	DbPass = "j29.!kl;2mzo8qomo.aslignyz;742ng2j:1u87<,1aq"
)

//GetMYSQLDBInstance return singleton object of DB Connection
func GetMYSQLDBInstance() *sql.DB {
	once.Do(func() {
		db = connectMysqlDB()
	})
	if db == nil {

		ErrorExit.ErrorPrint()
	}
	return db
}

//ConnectMysqlDB Method will accessible to all go files for connection
func connectMysqlDB() *sql.DB {
	dsn := DbUser + ":" + DbPass + "@" + DbHost + "/" + DbName + "?parseTime=true"
	fmt.Printf("DSN:::: %s\n", dsn)
	tempDB, err := sql.Open("mysql", dsn)
	err = tempDB.Ping()
	if err != nil {
		//log.Fatal(err)
		fmt.Printf("DB COnnection Error is:::: %s\n", err)
		return nil
	}
	fmt.Printf("Mysql Database connected successfully:::::::::\n")
	//defer db.Close()

	return tempDB

}
