package DbConnection

import (
	"database/sql"
	"fmt"
)

const (
	//DbNameAdminFitness1 : database name
	DbNameAdminFitness1 = "admin_fitness1"
)

var adminFitness1 sql.DB

//OpenDBAdminFitness1 will open the database connection
func OpenDBAdminFitness1() *sql.DB {
	//db, err := sql.Open("mysql", fmt.Sprintf("%s/%s/%s", DB_NAME, DB_USER, DB_PASS))
	adminFitness1, err := sql.Open("mysql", DbUser1+":"+DbPassword+"@/"+DbNameAdminFitness1)
	adminFitness1.SetMaxIdleConns(100)
	if err != nil {
		fmt.Println("error in opening database")
	}
	return adminFitness1
}

//CloseDBAdminFitness1 will close the database connection
func CloseDBAdminFitness1() {
	db1.Close()
}
