package main

import (
	"WSGoGarminFitbit/DbConnection"
	"WSGoGarminFitbit/Utils"
	"database/sql"
	"encoding/json"
	"log"
	"strings"
)

//structure for scanning rows of log_fitibit_mq table
type logFitbitMq struct {
	id        int
	ts        string
	data      string
	status    int
	statusNew int
}

var db *sql.DB

func main() {

	db = DbConnection.OpenDB()

	rows, err := db.Query("SELECT * FROM log_fitbit_mq order by id DESC limit 1")
	if err != nil {
		panic(err)
	}
	var allUpdates map[string]map[string][]string
	defer rows.Close()
	for rows.Next() {
		logFitbitMq := parseFitbitRaw(rows)
		fitbitUpdates := parseDataUploadFitbit(logFitbitMq)
		allUpdates = mergeUpdates(fitbitUpdates)

		updateStatus(logFitbitMq.id)
		log.Println(allUpdates)
		updateFitbitDaily(allUpdates["activities"])

		insertMetaFitbitSync(allUpdates)
	}

	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	ProcessMetaFitbitSyncQueue()
}

//insert data to meta_fitbit_sync_new table for further processing
func insertMetaFitbitSync(allUpdates map[string]map[string][]string) {
	for dataType, users := range allUpdates {
		for userID, dates := range users {
			for _, date := range dates {
				log.Println("date type : " + dataType + " user id : " + userID + " date : " + date)
				log.Println("Current Time : ", utils.GetCurrentTime())
				now := utils.GetCurrentTime()
				stmt, err := db.Prepare("INSERT INTO meta_fitbit_sync_new  SET uid=?, collection=?, ts_dirty=? ON DUPLICATE KEY UPDATE ts_dirty=GREATEST( VALUES(ts_dirty), ts_dirty )")
				if err != nil {
					log.Fatal(err)
				}

				_, err = stmt.Exec(userID, dataType, now)
				if err != nil {
					log.Fatal(err)
				}
			}
		}
	}
}
func updateFitbitDaily(allUpdates map[string][]string) {
	for user, dates := range allUpdates {
		log.Printf("user : [%s] dates : [%s]\n", user, dates)
		userDates := strings.Join(dates, ",")
		updateFitbitDailyDB(user, userDates)
	}
}

//set row to dirty in fitbit daily table
func updateFitbitDailyDB(user string, userDates string) {
	log.Printf("User : %s Dates : %s ", user, userDates)

	stmt, err := db.Prepare("UPDATE data_fitbit_daily SET is_dirty_new=1 WHERE uid=? AND dt IN (?)")
	if err != nil {
		log.Fatal(err)
	}

	_, err = stmt.Exec(user, userDates)
	if err != nil {
		log.Fatal(err)
	}
}

//update status field
func updateStatus(id int) {
	log.Println("id : ", id)
	stmt, err := db.Prepare("update log_fitbit_mq set status_new = 1 where id = ?")
	if err != nil {
		log.Fatal(err)
	}

	_, err = stmt.Exec(id)
	if err != nil {
		log.Fatal(err)
	}
}

//generate map of collection type which contains the map (map of user and dates)
//e.x ["activities"] {["user1"]"{date1,date2}",["user2"]"{date3,date2}"}
//    ["food"] {["user1"]"{date1,date3}",["user3"]"{date1,date4}"}
func mergeUpdates(fitbitUpdates []fitbitUpdate) map[string]map[string][]string {

	allUpdates := make(map[string]map[string][]string)
	for _, fitbitUpdate := range fitbitUpdates {

		log.Println("\n Fitbit Update ::", fitbitUpdate)
		// We only work with individual user packets.
		if fitbitUpdate.OwnerType != "user" {
			log.Printf("\nowner type id not user\n")
			continue
		} else {
			lastAllUpdates := make(map[string][]string)
			//get updates of particular collection type
			if allUpdates[fitbitUpdate.CollectionType] != nil {
				lastAllUpdates = allUpdates[fitbitUpdate.CollectionType]
			}
			//get particular user's updates for particular collection type
			dates := lastAllUpdates[fitbitUpdate.SubscriptionID]

			//add to eisting dates
			dates = append(dates, fitbitUpdate.Date)
			lastAllUpdates[fitbitUpdate.SubscriptionID] = dates

			//update particular collection type
			allUpdates[fitbitUpdate.CollectionType] = lastAllUpdates
		}
	}
	return allUpdates
}

//convert row to struct
func parseFitbitRaw(row *sql.Rows) *logFitbitMq {
	logFitbitMq := new(logFitbitMq)
	err := row.Scan(&logFitbitMq.id, &logFitbitMq.ts, &logFitbitMq.data, &logFitbitMq.status, &logFitbitMq.statusNew)
	if err != nil {
		log.Fatal(err)
	}
	return logFitbitMq
}

//structure for scanning "data" field of log_fitibit_mq table
type fitbitUpdate struct {
	CollectionType string `json:"collectionType"`
	Date           string `json:"date"`
	OwnerID        string `json:"ownerId"`
	OwnerType      string `json:"ownerType"`
	SubscriptionID string `json:"subscriptionId"`
}

func parseDataUploadFitbit(logFitbitMq *logFitbitMq) []fitbitUpdate {
	data := logFitbitMq.data
	var fitbitUpdates []fitbitUpdate

	err := json.Unmarshal([]byte(data), &fitbitUpdates)
	if err != nil {
		log.Fatal(err)
	}

	return fitbitUpdates
}
