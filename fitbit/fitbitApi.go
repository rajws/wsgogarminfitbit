package main

import (
	"WSGoGarminFitbit/DbConnection"
	"WSGoGarminFitbit/Utils"
	"database/sql"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"log"
	"net/http"
	"time"

	"golang.org/x/oauth2"
)

//XMLAuth is used for storing OAuth token details
type XMLAuth struct {
	// AccessToken is the token that authorizes and authenticates
	// the requests.
	AccessToken  string `xml:"access_token,attr"`
	RefreshToken string `xml:"refresh_token,attr,omitempty"`
	Expiry       string `xml:"token_expiry,attr,omitempty"`
	Type         string `xml:"_type,attr,omitempty"`
}

//convert token store in table to XML formate
func getFitbitAccessTokenXML(uID string) *XMLAuth {

	sqlStatement := `SELECT auth FROM users_sites WHERE uid=? and site_id=?`
	var auth string

	row := DbConnection.OpenDBAdminFitness1().QueryRow(sqlStatement, uID, utils.FITBIT_SITE_ID)
	switch err := row.Scan(&auth); err {
	case sql.ErrNoRows:
		return nil
	}

	var token XMLAuth
	if err := xml.Unmarshal([]byte(auth), &token); err != nil {
		panic(err)
	}

	fmt.Println("XMLToken :  ", token)
	return &token
}

//DateValue is used for storing information returned from fitbit
type DateValue struct {
	DateTime string `json:"dateTime"`
	Value    string `json:"value"`
}

//Data map will contain datetype and it's data(date and value)
type Data map[string][]DateValue

//query fitbit for data of dataType(steps,calorie,etc)
//isNonTrackerSteps - true for all data
//isNonTrackerSteps - false for only tracker data
func getFitbitData(dataType string, uID string, dateRange []string, isNonTrackerSteps bool) map[string][]DateValue {
	var path string
	var data Data
	auth := getFitbitAccessTokenXML(uID)
	token := getOAuthToken(auth)
	if auth == nil {
		fmt.Println("Auth not found for ", uID)
	} else {
		if isNonTrackerSteps {
			switch dataType {
			case "steps":
				path = "/activities/steps"
				break
			case "calories":
				path = "/activities/activityCalories"
				break
			case "distance":
				path = "/activities/distance"
				break
			case "minutesfairlyactive":
				path = "/activities/minutesFairlyActive"
				break
			case "minutesveryactive":
				path = "/activities/minutesVeryActive"
				break
			default:
				path = "/activities/steps"
				break
			}
		} else {
			switch dataType {
			case "steps":
				path = "/activities/tracker/steps"
				break
			case "calories":
				path = "/activities/tracker/activityCalories"
				break
			case "distance":
				path = "/activities/tracker/distance"
				break
			case "minutesfairlyactive":
				path = "/activities/tracker/minutesFairlyActive"
				break
			case "minutesveryactive":
				path = "/activities/tracker/minutesVeryActive"
				break
			default:
				path = "/activities/tracker/steps"
				break
			}
		}
	}
	fmt.Println("Path : " + path)
	fmt.Println("Access token : ", token.AccessToken)
	fmt.Println("Token Expiry : ", token.Expiry.Format(utils.YMD_HMS_FORMATE))

	url := utils.FITBIT_BASE_URL + "user/-" + path + "/date/" + dateRange[0] + "/" + dateRange[1] + ".json"
	fmt.Println("URL ::", url)

	/*var data = []byte(`
			{
	    "activities-steps":[
	        {"dateTime":"2011-04-27","value":5490},
	        {"dateTime":"2011-04-28","value":2344},
	        {"dateTime":"2011-04-29","value":2779},
	        {"dateTime":"2011-04-30","value":9196},
	        {"dateTime":"2011-05-01","value":15828},
	        {"dateTime":"2011-05-02","value":1945},
	        {"dateTime":"2011-05-03","value":366}
	    ]
	}
			`)
			var data1 = []byte(`
			{
	    "activities-activityCalories":[
	        {"dateTime":"2011-04-27","value":590},
	        {"dateTime":"2011-04-28","value":244},
	        {"dateTime":"2011-04-29","value":279},
	        {"dateTime":"2011-04-30","value":9196},
	        {"dateTime":"2011-05-01","value":15828},
	        {"dateTime":"2011-05-02","value":1945},
	        {"dateTime":"2011-05-03","value":366}
	    ]
	}
			`)

			var info Info
			err := json.Unmarshal([]byte(data), &info)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println("steps : ", info)

			var info1 Info
			err1 := json.Unmarshal([]byte(data1), &info1)
			if err1 != nil {
				log.Fatal(err1)
			}
			fmt.Println("calories : ", info1)
	*/

	client := getClient(token)
	res, err := client.Get(url)
	if err == nil {

		//for printing reponse
		/*bodyBytes1, err := ioutil.ReadAll(res.Body)

		if err == nil {

			bodyString1 := string(bodyBytes1)

			fmt.Printf("Response :", bodyString1)

		} else {
			log.Fatal(err)

		}*/

		decoder1 := json.NewDecoder(res.Body)
		err1 := decoder1.Decode(&data)
		if err1 != nil {
			log.Fatal(err1)
		}
	} else {
		log.Fatal(err)
	}

	return data
}

//query fitbit for tracker data of dataType(steps,calorie,etc)
func getTrackerFitbitData(dataType string, uID string, dateRange []string) map[string][]DateValue {
	var path string
	var data Data
	auth := getFitbitAccessTokenXML(uID)
	if auth == nil {
		fmt.Println("Auth not found for ", uID)
	} else {
		token := getOAuthToken(auth)
		switch dataType {
		case "steps":
			path = "/activities/tracker/steps"
			break
		case "calories":
			path = "/activities/tracker/activityCalories"
			break
		case "distance":
			path = "/activities/tracker/distance"
			break
		case "minutesfairlyactive":
			path = "/activities/tracker/minutesFairlyActive"
			break
		case "minutesveryactive":
			path = "/activities/tracker/minutesVeryActive"
			break
		default:
			path = "/activities/tracker/steps"
			break
		}

		fmt.Println("Path : " + path)
		fmt.Println("Access token : ", token.AccessToken)
		fmt.Println("Token Expiry : ", token.Expiry.Format(utils.YMD_HMS_FORMATE))

		url := utils.FITBIT_BASE_URL + "user/-" + path + "/date/" + dateRange[0] + "/" + dateRange[1] + ".json"
		fmt.Println("URL ::", url)

		/*var data = []byte(`
				{
		    "activities-steps":[
		        {"dateTime":"2011-04-27","value":5490},
		        {"dateTime":"2011-04-28","value":2344},
		        {"dateTime":"2011-04-29","value":2779},
		        {"dateTime":"2011-04-30","value":9196},
		        {"dateTime":"2011-05-01","value":15828},
		        {"dateTime":"2011-05-02","value":1945},
		        {"dateTime":"2011-05-03","value":366}
		    ]
		}
				`)
				var data1 = []byte(`
				{
		    "activities-activityCalories":[
		        {"dateTime":"2011-04-27","value":590},
		        {"dateTime":"2011-04-28","value":244},
		        {"dateTime":"2011-04-29","value":279},
		        {"dateTime":"2011-04-30","value":9196},
		        {"dateTime":"2011-05-01","value":15828},
		        {"dateTime":"2011-05-02","value":1945},
		        {"dateTime":"2011-05-03","value":366}
		    ]
		}
				`)

				var info Info
				err := json.Unmarshal([]byte(data), &info)
				if err != nil {
					log.Fatal(err)
				}
				fmt.Println("steps : ", info)

				var info1 Info
				err1 := json.Unmarshal([]byte(data1), &info1)
				if err1 != nil {
					log.Fatal(err1)
				}
				fmt.Println("calories : ", info1)
		*/

		client := getClient(token)
		res, err := client.Get(url)
		if err == nil {

			//for printing reponse
			/*bodyBytes1, err := ioutil.ReadAll(res.Body)

			if err == nil {

				bodyString1 := string(bodyBytes1)

				fmt.Printf("Response :", bodyString1)

			} else {
				log.Fatal(err)

			}*/

			decoder1 := json.NewDecoder(res.Body)
			err1 := decoder1.Decode(&data)
			if err1 != nil {
				log.Fatal(err1)

			}
			//fmt.Println("data : ", data)

		} else {
			log.Fatal(err)
		}
	}

	return data
}

//oAuthToken object
type tokenSource struct{ token *oauth2.Token }

//get httpclient for doing API calls
func getClient(oAuthToken *oauth2.Token) *http.Client {
	tr := &oauth2.Transport{
		Source: utils.GetFitbitConfig().TokenSource(oauth2.NoContext, oAuthToken)}
	client := &http.Client{Transport: tr}
	return client
}

//convert XML token to OAuth token object
func getOAuthToken(xmlAuth *XMLAuth) *oauth2.Token {
	var token oauth2.Token
	token.AccessToken = xmlAuth.AccessToken
	token.RefreshToken = xmlAuth.RefreshToken
	token.Expiry, _ = time.Parse(utils.YMD_HMS_FORMATE, xmlAuth.Expiry)
	return &token
}
