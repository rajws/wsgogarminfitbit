package main

import (
	"WSGoGarminFitbit/DbConnection"
	"WSGoGarminFitbit/Utils"
	"database/sql"
	"log"
	"math/rand"
	"strconv"
	"time"
)

//structure for scanning rows of meta_fitbit_sync_new table
type metaFitbitSync struct {
	uID        int
	collection string
	tsClean    int64
	tsDirty    int64
	isDirty    int
}

//structure for scanning rows of xAttr table
type xAttr struct {
	cls string
	id  int
	k   string
	v   string
}

var dbTemp *sql.DB
var dbAdminFitness1 *sql.DB

//ProcessMetaFitbitSyncQueue is used for processing meta_fitbit_sync_new table
func ProcessMetaFitbitSyncQueue() {

	dbTemp = DbConnection.OpenDB()
	dbAdminFitness1 = DbConnection.OpenDBAdminFitness1()

	rowsAdminFitness1, errAdminFitness1 := dbAdminFitness1.Query("SELECT GROUP_CONCAT(id) AS nontracker_groups FROM xattr WHERE cls='group' AND k='fitbit/non-tracker-steps/enable' AND v='1'")

	if errAdminFitness1 != nil {
		panic(errAdminFitness1)
	}

	//go doesn't have contains method,so searching in array takes more time,so instead take map,and take dummy type as map value,here,we have taken bool
	uIDs := make(map[string]bool)
	defer rowsAdminFitness1.Close()
	for rowsAdminFitness1.Next() {
		nonTrackerGroups := parseXattr(rowsAdminFitness1)

		log.Println("non tracker groups ", nonTrackerGroups)

		if nonTrackerGroups != "" {
			rowsGroupMembers, errGroupMembers := dbAdminFitness1.Query("SELECT uid FROM fitness_group_members WHERE gid IN (" + nonTrackerGroups + ")")

			if errGroupMembers != nil {
				panic(errGroupMembers)
			}

			defer rowsGroupMembers.Close()
			for rowsGroupMembers.Next() {
				uID := parseFitnessGroupMembers(rowsGroupMembers)
				//add uID to map
				uIDs[strconv.Itoa(uID)] = false
			}
			log.Println("uIDs : ", uIDs)
			errGroupMembers = rowsGroupMembers.Err()
			if errGroupMembers != nil {
				log.Fatal(errGroupMembers)
			}
		}
	}

	errAdminFitness1 = rowsAdminFitness1.Err()
	if errAdminFitness1 != nil {
		log.Fatal(errAdminFitness1)
	}

	rows, err := dbTemp.Query("SELECT * FROM meta_fitbit_sync_new WHERE MOD(uid, 1)=0  and uid=503484 ORDER BY ts_dirty DESC LIMIT 1")

	if err != nil {
		panic(err)
	}

	defer rows.Close()
	for rows.Next() {
		isNonTrackerSteps := false
		metaFitbitSync := parseMetaFitbitSyncRaw(rows)
		uID := strconv.Itoa(metaFitbitSync.uID)
		//log.Println(" uId : " + strconv.Itoa(metaFitbitSync.uID) + " collection : " + metaFitbitSync.collection + " tsClean : " + uint64(metaFitbitSync.tsClean) + " tsDirty : " + uint64(metaFitbitSync.tsDirty) + " isDirty : " + strconv.Itoa(metaFitbitSync.isDirty))
		log.Println(" uId : " + uID + " collection : " + metaFitbitSync.collection + " tsClean : " + " isDirty : " + strconv.Itoa(metaFitbitSync.isDirty))
		//fitbitUpdates := parseDataUploadFitbit(logFitbitMq)
		if _, ok := uIDs[uID]; ok {
			// do something
			isNonTrackerSteps = true
		}
		processMetaFitbitSync(uID, isNonTrackerSteps)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

}

//convert meta_fitbit_sync_new table row to struct
func parseMetaFitbitSyncRaw(row *sql.Rows) *metaFitbitSync {
	metaFitbitSync := new(metaFitbitSync)
	err := row.Scan(&metaFitbitSync.uID, &metaFitbitSync.collection, &metaFitbitSync.tsClean, &metaFitbitSync.tsDirty, &metaFitbitSync.isDirty)
	if err != nil {
		log.Fatal(err)
	}

	return metaFitbitSync
}

//convert xAttr table row to struct
func parseXattr(row *sql.Rows) string {
	var nonTrackerGroups string
	err := row.Scan(&nonTrackerGroups)
	if err != nil {
		log.Fatal(err)
	}

	return nonTrackerGroups
}

//get uid from row
func parseFitnessGroupMembers(row *sql.Rows) int {
	var uID int
	err := row.Scan(&uID)
	if err != nil {
		log.Fatal(err)
	}

	return uID
}

//process individual record of meta_fitbit_sync_new table
func processMetaFitbitSync(uID string, isNonTrackerSteps bool) {
	log.Println("uID: ", uID, " isNonTrackerSteps : ", isNonTrackerSteps)
	timezone := getTimeZone(uID)
	log.Println("time Zone : ", timezone)
	dateRange := getDateRange(uID, timezone)
	log.Println("Range Start: ", dateRange[0])
	log.Println("Range End: ", dateRange[1])
	getAllUserData(uID, dateRange, isNonTrackerSteps)
}

//get date range for which data has changed for user

func getDateRange(uID string, timeZone int64) []string {

	//dateRange[0]-start date , dateRange[1]-end date
	dateRange := make([]string, 2)

	currentTimeSeconds := utils.GetCurrentTimeSeconds()
	currentTimeSecondsTimeZone := currentTimeSeconds + timeZone

	currentTime := time.Unix(currentTimeSecondsTimeZone, 0)
	timeBeforeMaxDays := currentTime.AddDate(0, 0, -utils.MAX_FITBIT_DAYS)

	strCurrentTime := currentTime.Format(utils.YMD_FORMATE)
	strtimeBeforeMaxDays := timeBeforeMaxDays.Format(utils.YMD_FORMATE)

	log.Println("Current time : ", strCurrentTime)
	log.Println("Time before max fitibit days : ", strtimeBeforeMaxDays)

	dateRange[0] = strtimeBeforeMaxDays
	dateRange[1] = strCurrentTime
	lastday := getLastDataDate(uID, strCurrentTime)
	log.Println("Last Day : ", lastday)

	if lastday != "" {
		lastDayTime, _ := time.Parse(utils.YMD_FORMATE, lastday)
		if lastDayTime.Before(timeBeforeMaxDays) {
			dateRange[0] = strtimeBeforeMaxDays
		} else {
			dateRange[0] = lastday
		}
	}

	return dateRange
}

//get user's timezone from table
func getTimeZone(uID string) int64 {
	sqlStatement := `SELECT timezone FROM users WHERE uid=?`
	var timezone int64

	row := dbAdminFitness1.QueryRow(sqlStatement, uID)
	switch err := row.Scan(&timezone); err {
	case sql.ErrNoRows:
		log.Println("No timezone for user " + uID)
		return 0
	}
	return timezone
}

//get last sync date of user
func getLastDataDate(uID string, strCurrentTime string) string {
	sqlStatement := `SELECT MAX(date) FROM raw_fitbit_stepping WHERE uid = ? AND date <= ?`
	var date string

	row := dbTemp.QueryRow(sqlStatement, uID, strCurrentTime)
	switch err := row.Scan(&date); err {
	case sql.ErrNoRows:
		log.Println("No date for user from raw_fitbit_stepping" + uID)
		return ""
	}
	return date
}

//get steps,calorie,etc data of users from fitbit
func getAllUserData(uID string, dateRange []string, isNonTrackerSteps bool) {
	dateUserData := make(map[string]map[string]string)
	log.Println("getAllUserData called....")
	dateUserData = getUserData("steps", uID, dateRange, dateUserData, isNonTrackerSteps)
	log.Println("dateUserData : ", dateUserData)
	dateUserData = getUserData("calories", uID, dateRange, dateUserData, isNonTrackerSteps)
	log.Println("dateUserData : ", dateUserData)
	dateUserData = getUserData("distance", uID, dateRange, dateUserData, isNonTrackerSteps)
	log.Println("dateUserData : ", dateUserData)
	dateUserData = getUserData("minutesfairlyactive", uID, dateRange, dateUserData, isNonTrackerSteps)
	log.Println("dateUserData : ", dateUserData)
	dateUserData = getUserData("minutesveryactive", uID, dateRange, dateUserData, isNonTrackerSteps)
	log.Println("dateUserData : ", dateUserData)
	insertRawData(uID, dateUserData)
}

func getUserData(dataType string, uID string, dateRange []string, dateUserData map[string]map[string]string, isNonTrackerSteps bool) map[string]map[string]string {

	data := getFitbitData(dataType, uID, dateRange, isNonTrackerSteps)
	if data != nil {
		for _, v := range data {

			for _, element := range v {
				// index is the index where we are
				// element is the element from someSlice for where we are
				log.Printf("date : %s value : %s \n", element.DateTime, element.Value)
				dataUpdates := make(map[string]string)
				if dateUserData[element.DateTime] != nil {
					dataUpdates = dateUserData[element.DateTime]
				}
				dataUpdates[dataType] = element.Value
				dateUserData[element.DateTime] = dataUpdates
			}
		}
	}
	return dateUserData
}

//insert steps,etc data of user to riw_fitbit_stepping_new table
func insertRawData(uID string, dateUserData map[string]map[string]string) {
	for date, v := range dateUserData {
		log.Println("Date : ", date)
		steps := v["steps"]
		calories := v["calories"]
		distance := v["distance"]
		minutesFairlyActive := v["minutesfairlyactive"]
		minutesVeryActive := v["minutesveryactive"]
		logID := rand.Int31n(1000000)
		log.Printf("logId : %d date : %s steps : %s calories %s distance : %s minutesFairlyActive : %s minutesVeryActive : %s \n", logID, date, steps, calories, distance, minutesFairlyActive, minutesVeryActive)
		stmt, err := db.Prepare("INSERT INTO raw_fitbit_stepping_new SET logId= ? , type=?, uid=?, date=?, time=?, steps=?,distance=?, calories=?, duration=?, packet=?, status=?")

		if err != nil {
			log.Fatal(err)
		}

		_, err = stmt.Exec(logID, 1, uID, date, "00:00", steps, distance, calories, minutesFairlyActive, "test", 1)
		if err != nil {
			log.Fatal(err)
		}
	}
}
