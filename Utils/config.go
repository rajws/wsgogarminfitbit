package utils

import (
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/fitbit"
)

//GetFitbitConfig returns configuration object of production
/*func GetFitbitConfig() *oauth2.Config {
	conf := &oauth2.Config{
		ClientID:     "228S2Y",
		ClientSecret: "3788ee3174f04455a5d6f3597f42950a",
		Scopes:       []string{"activity", "nutrition", "profile", "settings", "social", "weight"},
		Endpoint:     fitbit.Endpoint,
		RedirectURL:  "http://members-new.staging.walkingspree.com/app/fitbit/oauth2-callback",
	}
	return conf
}*/

//GetFitbitConfig returns configuration object of staging
func GetFitbitConfig() *oauth2.Config {
	conf := &oauth2.Config{
		ClientID:     "227PP8",
		ClientSecret: "4f6f1d9791a734e505ef7564a9e972c2",
		Scopes:       []string{"activity", "nutrition", "profile", "settings", "social", "weight"},
		Endpoint:     fitbit.Endpoint,
		RedirectURL:  "http://members-new.staging.walkingspree.com/app/fitbit/oauth2-callback",
	}
	return conf
}
