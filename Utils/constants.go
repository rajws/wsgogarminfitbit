package utils

const YMD_FORMATE string = "2006-01-02"
const MAX_FITBIT_DAYS int = 30
const FITBIT_SITE_ID int = 2
const YMD_HMS_FORMATE string = "2006-01-02 15:04:05"
const API_HOST string = "api.fitbit.com"
const FITBIT_BASE_URL = "https://" + API_HOST + "/1/"
