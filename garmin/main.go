//oauth authentication call file is data_garmin.resources.garmin.api.inc and method is data_garmin_api_backfill_summaries
package main

import (
	"WSGoGarminFitbit/DbConnection"
	"fmt"
	"database/sql"
	"log"
	//"fmt"
	"time"
	//"github.com/dghubble/oauth1"
	//"io/ioutil"
	//"github.com/dghubble/oauth1"
)

type garminPing struct {
	id         int
	ts         time.Time
	data       string
	status     int
	collection string
}

type pingDataFormat struct {
	UserAccessToken          string `json:"userAccessToken"`
	UploadStartTimeInSeconds int    `json:"uploadStartTimeInSeconds"`
	UploadEndTimeInSeconds   int    `json:"uploadEndTimeInSeconds"`
	CallbackURL              string `json:"callbackURL"`
}

type dailySummary struct {
	summaryID                        string         `json:"summaryId"`
	activityType                     string         `json:"activityType"`
	activeKilocalories               float32        `json:"activeKilocalories"`
	steps                            int            `json:"steps"`
	distanceInMeters                 float32        `json:"distanceInMeters"`
	durationInSeconds                int            `json:"durationInSeconds"`
	activeTimeInSeconds              int            `json:"activeTimeInSeconds"`
	startTimeInSeconds               int            `json:"startTimeInSeconds"`
	startTimeOffsetInSeconds         int            `json:"startTimeOffsetInSeconds"`
	minHeartRateInBeatsPerMinute     int            `json:"minHeartRateInBeatsPerMinute"`
	maxHeartRateInBeatsPerMinute     int            `json:"maxHeartRateInBeatsPerMinute"`
	averageHeartRateInBeatsPerMinute int            `json:"averageHeartRateInBeatsPerMinute"`
	restingHeartRateInBeatsPerMinute int            `json:"restingHeartRateInBeatsPerMinute"`
	timeOffsetHeartRateSamples       map[string]int `json:"timeOffsetHeartRateSamples"`
	stepsGoal                        int            `json:"stepsGoal"`
	intensityDurationGoalInSeconds   int            `json:"intensityDurationGoalInSeconds"`
	floorsClimbedGoal                int            `json:"floorsClimbedGoal"`
}

var mysqlDB *sql.DB

//GarminConsumerKey is constant variable which contain garmin consumer key for oauth
const GarminConsumerKey string = "312f93fe-d16b-4161-80be-fe1c764902a9"

//GarminConsumerSecret is constant variable which contain garmin Garmin secret key for oauth
const GarminConsumerSecret string = "BOYkaFfdmrevgX26et3z49CZN3aX9rv2V9R"

//GarminAuthHost is constant variable which point to testing environment
const GarminAuthHost string = "connecttest.garmin.com"

//GarminAPIHost is constant variable which point to testing environment
const GarminAPIHost string = "gcsapitest.garmin.com"

//GarminConnectAPIHost is constant variable which point to testing environment
const GarminConnectAPIHost string = "connectapitest.garmin.com"

func main() {

	// config := oauth1.Config{
	// 	ConsumerKey:    "312f93fe-d16b-4161-80be-fe1c764902a9",
	// 	ConsumerSecret: "BOYkaFfdmrevgX26et3z49CZN3aX9rv2V9R",
	// 	CallbackURL:    "http://localhost:7319/",
	// 	Endpoint: oauth1.Endpoint{
	// 		RequestTokenURL: "https://connectapitest.garmin.com/oauth-service-1.0/oauth/request_token",
	// 		AuthorizeURL:    "https://connecttest.garmin.com/oauthConfirm",
	// 		AccessTokenURL:  "https://connectapitest.garmin.com/oauth-service-1.0/oauth/access_token",
	// 	},
	// }

	// //This will return request token and Secret
	// requestToken, requestSecret, err := config.RequestToken()

	// if err != nil {

	// 	fmt.Printf("Request Token Error::: %v\n", err)
	// }

	// fmt.Printf("request Token::: %v\n", requestToken)
	// fmt.Printf("request secret::: %v\n", requestSecret)

	// //Using above request token and secret generate Authorize url,(Combination of end point authorize url and request token)
	// //Run that url on browser allow permission, which will return verifier code
	// authorizationURL, err := config.AuthorizationURL(requestToken)
	// if err != nil {
	// 	fmt.Printf("Authorize Url Error::: %v\n", err)
	// }
	// fmt.Printf("Open this URL in your browser:\n%s\n", authorizationURL)

	// //Access token and secret key will generate at callback url, but testing purpose you can copy required parameter and run.
	// //Comment required token and authrize url code and uncomment access token code to test
	// //accessToken, accessSecret, err := config.AccessToken(requestToken, requestSecret, verifier)
	// accessToken, accessSecret, err := config.AccessToken("1114526e-94e9-4b0e-b9cb-79ccb86cb6ec", "wsvmFoRXwtIvbKEwaxy8gRK7F95UjSMK1b8", "JnGaWGGDRn")
	// if err != nil {
	// 	fmt.Printf("Access token Error::: %v\n", err)
	// }

	// fmt.Println("Consumer was granted an access token to act on behalf of a user.")
	// fmt.Printf("token: %s\nsecret: %s\n", accessToken, accessSecret)

	mysqlDB = DbConnection.GetMYSQLDBInstance()

	rows, error := mysqlDB.Query("SELECT * FROM log_garmin_pings ORDER BY id DESC LIMIT 1")
	if error != nil {
		log.Fatal(error)
	}
	defer rows.Close()

	fmt.Print("Rows:::: %v",rows)

	// var pingData string

	// for rows.Next() {

	// 	pingRecord := new(garminPing)
	// 	err := rows.Scan(&pingRecord.id, &pingRecord.ts, &pingRecord.data, &pingRecord.status, &pingRecord.collection)
	// 	if err != nil {
	// 		fmt.Printf("Error is::: %v", err)
	// 		return
	// 	}

	// 	pingData = pingRecord.data
	// }

	// pingJSON := make(map[string][]pingDataFormat)

	// err := json.Unmarshal([]byte(pingData), &pingJSON)

	// if err != nil {
	// 	panic(err)
	// }

	// fmt.Printf("\n\n json object:::: %v", pingJSON)

	// dailies := pingJSON["dailies"]

	// //Authrize request
	// config := oauth1.NewConfig("312f93fe-d16b-4161-80be-fe1c764902a9", "BOYkaFfdmrevgX26et3z49CZN3aX9rv2V9R")
	// token := oauth1.NewToken("2268867c-13ee-44ec-b927-e58699abe6b6", "LmbTl7zHOp9YcagcjMWLVbekmsnxWj28tG3")

	// // httpClient will automatically authorize http.Request's
	// httpClient := config.Client(oauth1.NoContext, token)

	// // Fetch garmin dailies data
	// path := dailies[0].CallbackURL
	// resp, _ := httpClient.Get(path)
	// // defer resp.Body.Close()
	// // body, _ := ioutil.ReadAll(resp.Body)
	// fmt.Printf("\nRaw Response Body:\n%v\n", resp)

}
